**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

---

## Getting Started

1. Clone the repository from Bitbucket

```bash
 git clone https://joaquin-mancheno@bitbucket.org/joaquin-mancheno/api-challenge.git
```

2. Switch to the repo folder

```bash
 cd api-challenge
```

3. Install dependencies

```bash
cd api-challenge
```

4. Start the local server

```bash
npm start
```

## Test Api

Valid Inputs :

- Input-temperture
- Input-units
- Target-units
- Student-response

Test Api with http://localhost:5000/{Input-temperture}/{Input-units}/{Target-units}/{Student-response}

Example

84.2 Fahrenheit to Rankine -- student response 543.5

http://localhost:5000/84.2/Fahrenheit/Rankine/543.5

returns:

{
"input_temperature": 84.2,
"input_unit": "fahrenheit",
"target_unit": "rankine",
"student_answer": 544,
"grade": "correct"
}

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
