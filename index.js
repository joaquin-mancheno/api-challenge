const express = require("express");

const app = express(``);

app.get(
  "/:inputNumber/:inputUnit/:targetUnit/:studentAnswer",
  async (req, res) => {
    try {
      //input temperture
      const temperature = await Number(req.params.inputNumber);
      if (isNaN(temperature)) {
        res.status(500).json(`${temperature} is not valid`);
      } else {
        console.log(temperature);
      }

      //input unit
      const inputUnit = await req.params.inputUnit.toString().toLowerCase();

      //validate that the inputUnit is Fahrenheit, Celsius or Kelvin
      if (
        inputUnit === "rankine" ||
        inputUnit === "celsius" ||
        inputUnit === "kelvin" ||
        inputUnit === "fahrenheit"
      ) {
        console.log(inputUnit);
      } else res.status(500).json(`${inputUnit} is not a valid input`);

      //Target unit-test"

      const targetUnit = await req.params.targetUnit.toString().toLowerCase();

      if (
        targetUnit === "rankine" ||
        targetUnit === "celsius" ||
        targetUnit === "kelvin" ||
        targetUnit === "fahrenheit"
      ) {
        console.log(targetUnit);
      } else res.status(500).json(`${targetUnit} is not a valid input`);

      const studentAnswer = await Math.round(Number(req.params.studentAnswer));

      if (isNaN(studentAnswer)) {
        res.status(500).json(`${studentAnswer} is not a valid`);
      } else {
        console.log(studentAnswer);
      }

      var studentResponse;

      //Rankine

      if (inputUnit === "rankine") {
        if (targetUnit === "fahrenheit") {
          studentResponse = rankineToFahrenheit(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        } else if (targetUnit === "celsius") {
          studentResponse = rankineToCelsius(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        } else if (targetUnit === "kelvin") {
          studentResponse = rankineToKelvin(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        }
      }

      //farerenheit

      if (inputUnit === "fahrenheit") {
        if (targetUnit === "rankine") {
          studentResponse = fahrenheitToRankine(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        } else if (targetUnit === "celsius") {
          studentResponse = fahrenheitToCelsius(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        } else if (targetUnit === "kelvin") {
          studentResponse = fahrenheitToKelvin(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        }
      }

      //kelvin

      if (inputUnit === "kelvin") {
        if (targetUnit === "rankine") {
          studentResponse = kelvinToRankine(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        } else if (targetUnit === "celsius") {
          studentResponse = kelvinToCelsius(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        } else if (targetUnit === "fahrenheit") {
          studentResponse = kelvinToFahrenheit(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        }
      }

      //celsius

      if (inputUnit === "celsius") {
        if (targetUnit === "rankine") {
          studentResponse = celsiusToRankine(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        } else if (targetUnit === "fahrenheit") {
          studentResponse = celsiusToFahrenheit(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        } else if (targetUnit === "kelvin") {
          studentResponse = celsiusToKelvin(
            temperature,
            inputUnit,
            targetUnit,
            studentAnswer
          );
        }
      }

      const answer = {
        input_temperature: temperature,
        input_unit: inputUnit,
        target_unit: targetUnit,
        student_answer: studentAnswer,
        grade: studentResponse,
      };

      res.status(200).json(answer);
    } catch (error) {
      console.log(error);
    }
  }
);

//rankine functions

const rankineToFahrenheit = (
  temperature,
  inputUnit,
  targetUnit,
  studentAnswer
) => {
  const formula = Math.round(temperature - 458.67);
  console.log(temperature);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

const rankineToCelsius = (
  temperature,
  inputUnit,
  targetUnit,
  studentAnswer
) => {
  const formula = Math.round((temperature - 491.67) * (5 / 9));
  console.log(temperature);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

const rankineToKelvin = (temperature, inputUnit, targetUnit, studentAnswer) => {
  const formula = Math.round(temperature * (5 / 9));
  console.log(formula);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

//fahrenheit functions

const fahrenheitToRankine = (
  temperature,
  inputUnit,
  targetUnit,
  studentAnswer
) => {
  const formula = Math.round(temperature + 459.67);
  console.log(formula);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

const fahrenheitToCelsius = (
  temperature,
  inputUnit,
  targetUnit,
  studentAnswer
) => {
  const formula = Math.round((temperature - 32) * (5 / 9));

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

const fahrenheitToKelvin = (
  temperature,
  inputUnit,
  targetUnit,
  studentAnswer
) => {
  const formula = Math.round((temperature - 32) * (5 / 9) + 273);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

//kelvin functions

const kelvinToRankine = (temperature, inputUnit, targetUnit, studentAnswer) => {
  const formula = Math.round(temperature * 1.8);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

const kelvinToCelsius = (temperature, inputUnit, targetUnit, studentAnswer) => {
  const formula = Math.round(temperature - 273.15);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

const kelvinToFahrenheit = (
  temperature,
  inputUnit,
  targetUnit,
  studentAnswer
) => {
  const formula = Math.round(((temperature - 273.15) * 9) / 5 + 32);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

//celsius

const celsiusToRankine = (
  temperature,
  inputUnit,
  targetUnit,
  studentAnswer
) => {
  const formula = Math.round((temperature + 273.15) * (9 / 5));

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

const celsiusToKelvin = (temperature, inputUnit, targetUnit, studentAnswer) => {
  const formula = Math.round(temperature + 273.15);
  console.log(formula + "<---");

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

const celsiusToFahrenheit = (
  temperature,
  inputUnit,
  targetUnit,
  studentAnswer
) => {
  const formula = Math.round(temperature * (9 / 5) + 32);
  console.log(formula);

  if (studentAnswer === formula) {
    result = "correct";
  } else {
    result = "incorrect";
  }

  return result;
};

app.listen(5000, () => {
  console.log(`Running in port 5000`);
});
